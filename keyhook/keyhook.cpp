// keyhook.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

const char *in_diacritics[MAX_CHAR][MAX_LEN] = {"a", "aw", "aa", "b", "c", "d", "dd", "e", "ee", "g", "h", "i", "k", "l", "m", "n", "o", "oo", "ow", "p", "q", "r", "s", "t", "u", "uw", "v", "x", "y", "w", "z", "j"};
const char *out_diacritics[MAX_CHAR][MAX_LEN] = {"4", "4", "4", "|3", "c", "|)", "+)", "3", "3", "g", "h", "j", "k", "|_", "m", "n", "0", "0", "0", "|3", "w", "z", "s", "t", "u", "u", "v", "x", "ij", "w", "z", "j"};

const char in_tone[MAX_CHAR] = {"s", "f", "r", "x", "j"};
const char out_tone[MAX_CHAR][MAX_LEN] = {"'", "`", "?", "~", "."}; 
const int MAX_BUFF_SIZE = 128;
char buffer[MAX_CHAR];
char oldKey;
int oldSize, curSize;
bool  shitfKeyDown;
HWND currentHwnd;
/*
commit key first -> not in (0x41 -> 0x5A): reset buffer_in, buffer_out
tone second -> if (second_vowel_position) -> update_tone(buffer_in, buffer_out, new tone)
combine key third -> if (can combine with each char in buffer_in) -> update_key(buffer_in, buffer_out, new char)
itself -> search in diracritics, update_key(buffer_in, buffer_out, new char)


update_tone:
	if (curSize == 0) -> update_key
	if curSize != 0 -> insert(buffer, out_tone[new key], oldSize, curSize)
update_key:
	//change key
	if (oldKey + new char) in in_diacritics
		replace(buffer, out_diacritics[oldKey], outdiacritics[oldKey + new char], oldSize, curSize)
	else if (new char in in_diacritics)
		insert(buffer, outdiacritics[new char], oldSize, curSize)


	
*/
UINT VK_BACK_SCAN = MapVirtualKey(VK_BACK, 0);

void resetBuffer()
{
	second_vowel_position = 0;
	memset(buffer, sizeof(buffer), 0);
	first_vowel = true;
}

/*
get index of string in array
input: 
string: string to check
arr: arr to check if string in this
output:
-1: not in array
an integer: index of string in array
*/
int indexInArray(char *string, char* arr[MAX_CHAR][MAX_LEN])
{
	for (int i = 0; i < MAX_CHAR; i++)
		if (!strcmp(string, arry[i]))
			return true;
	return false;
}

/*
append string to buffer
The function check if buffer has enough space and add string to it
input:
buffer: buffer to add string. Value in buffer will be modified
string: string to add to buffer
output:
return true if successful
otherwise, return 0
*/
bool addStrToArr(char **buffer, char *string)
{
	if (MAX_BUFF_SIZE - strlen(*buffer) > strlen(string)
		return strcat(*buffer, string);
	return 0;
}
/*
encode key from vk_keycode and push it to buffer
check if virtual keycode is alphabet: 0x41-0x5A
input:
buffer: buffer to process
vk_keycode: keycode
output:
return the size of buffer before encode
return -1 if error
*/
int encode(char **buffer, DWORD vk_keycode)
{
	if (0x40 < vk_keycode && vk_keycode < 0x5A)
	{
		int size = strlen(*buffer);
		if (addStrToArr(buffer, &(char)vk_keycode))
			return size;
	}
	return -1;
}
/*
process special case of keycode
input:
keycode
output:
return 1 if key code is pre-process
return 0 otherwise
*/
bool preProcessVk_keycode(DWORD vk_keycode)
{
	switch(vk_keycode)
	{
	case VK_SHIFT:
		return shitfKeyDown = true;
		break;
	}
	return 0;
}

/*
hook function
get key input to hwnd and process it to teen code

if shift -> uppper key
get key and encode it, add to buffer -> return callnexthook if key not in 0x41-0x5A or shift key
if key is tone marked -> add tone maker to buffer
else
if key is vowel -> check if is first vowel, if not set its position to second_vowel_position
else
if key is commit key 
-> send buffer to hwnd
-> reset buffer
*/
__declspec(dllexport) LRESULT  WINAPI keyhook(int nCode, WPARAM wParam, LPARAM lParam) 
{
	if (nCode != 0)
		return CallNextHookEx(NULL, nCode, wParam, lParam); 

	KBDLLHOOKSTRUCT* mystruct = (KBDLLHOOKSTRUCT*) lParam;
	if (wParam == WM_KEYDOWN)
	{

		if (preProcessVk_keycode(mystruct->vkCode))
		{
			backs == encode(buffer, mystruct->vkCode);
			if (backs > -1)
				for (int i = 0; i < backs; i++) {
					keybd_event(VK_BACK, VK_BACK_SCAN,0,0);
					keybd_event(VK_BACK, VK_BACK_SCAN, KEYEVENTF_KEYUP, 0);
				}
		}
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam); 
}